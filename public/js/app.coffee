(($) ->
  
  # MODEL
  Task = Backbone.Model.extend()
  
  # ----------------------------------------------------------------------------
  
  # COLLECTION
  
  Tasks = Backbone.Collection.extend
	  model: Task,
  	url: "/tasks"
  
  # ----------------------------------------------------------------------------
  
  # TASKS VIEW 
  class TasksView extends Backbone.View
    
    el: $("#main")
    template: Handlebars.compile($("#tasksTemplate").html())

    events:
      "submit #add": "addTask",
      
    
    initialize: ->
      @collection = new Tasks()
      @collection.fetch()
      @collection.on "reset", @render, this
      @collection.on "add", @appendTask, this


    render: ->
      $(@el).html @template()
      @collection.each(@appendTask)

    appendTask: (task) ->
      task_view = new TaskView(model: task)
      $("#tasks").append($(task_view.render().el).hide().fadeIn())
      $("form#add")[0].reset()

    addTask: ->
      event.preventDefault()
      attributes = 
      	title: $("form#add input[name=title]").val()
      	content: $("form#add input[name=content]").val()  
      @collection.create attributes,
      	wait: true
      	success: (task , response) -> 
      		console.log task.get('title')
      		$(".success").text('').html("Task <strong>" + task.get('title') + "</strong> został dodany").slideDown().delay(4000).slideUp()
      	error: (task , response)-> 
      		if response.status == 422
      			$(".errors").text('')
      			for error in $.parseJSON(response.responseText)
      				$(".errors").append(error + "<br>")
      			$(".errors").slideDown().delay(4000).slideUp()
  
  # ----------------------------------------------------------------------------
  
  # TASK VIEW
  TaskView = Backbone.View.extend(
    template: Handlebars.compile($("#taskTemplate").html())
    tagName: "tr"

    events: 
    	"click a.destroy": "destroyTask"
    	"dblclick td": "editTask"
    
    initialize: ->
	    @model.on "destroy", @removeTask, this

    destroyTask: ->
    	console.log "destroy fired"
    	@model.destroy 
	    	wait: true

    removeTask: ->
    	$(@el).css(color: 'red').fadeOut() 

    render: ->
      $(@el).html @template(task: @model.toJSON())
      this

    editTask:	->

    	$(@el).toggleClass('editing').siblings("tr").removeClass('editing')
  )
  
  # ----------------------------------------------------------------------------
  
  # ROUTER
  AppRouter = Backbone.Router.extend(
    routes:
      "": "index"

    initialize: ->

    index: ->
      tasks_view = new TasksView()
      $("#main").html tasks_view.render()
  )
  
  # ----------------------------------------------------------------------------


  router = new AppRouter()
  Backbone.history.start()

) jQuery