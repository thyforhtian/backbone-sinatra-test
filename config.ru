require 'rack/coffee'
require './main.rb'

use Rack::Coffee,
	:root => "public", 
	:urls => '/js'  

run App
