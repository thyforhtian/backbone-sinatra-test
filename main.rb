require 'sinatra'
require 'data_mapper'
require 'json'

configure do
	DataMapper::Logger.new($stdout, :debug)

	DataMapper.setup(:default, 'postgres://sin_user:sin_2013_user@localhost/sin')

	class Task  
	  include DataMapper::Resource  
	  property :id, Serial  
	  property :title, Text, :required => true
	  property :content, Text, :required => true  
	  property :created_at, DateTime  
	  property :updated_at, DateTime  
	end  

	DataMapper.finalize.auto_upgrade!
end

class App < Sinatra::Base

	get '/' do
		@tasks = Task.all
		haml :index
	end

	get '/tasks' do
		content_type :json
		@tasks = Task.all.to_json
	end

	post '/tasks' do
		content_type :json
		data = JSON.parse(request.body.read)
		new_task = Task.new(data)
		if new_task.save
			return new_task.to_json
		else
			errors = []
			new_task.errors.each do |e|
				errors << e.join
			end
			status 422
			errors.to_json
		end	
	end

	put '/tasks/:id' do
		content_type :json
		data = JSON.parse(request.body.read)
		updated_task = Task.get(params[:id])
		updated_task.update(data)
	end

	delete '/tasks/:id' do
		content_type :json
		task = Task.get(params[:id])
		cached_task = task.to_json
		return cached_task if task.destroy
	end
end



