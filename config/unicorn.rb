@dir = "/usr/share/nginx/html/sin/current/"

worker_processes 1
user 'deployer'

preload_app true
timeout 30

working_directory @dir
listen "/tmp/sin.sock", :backlog => 64

pid "#{@dir}tmp/pids/sin.pid"
stderr_path "#{@dir}log/sin.stderr.log"
stdout_path "#{@dir}log/sin.stdout.log"

before_fork do |server, worker|

  old_pid = "#{server.config[:pid]}.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end
