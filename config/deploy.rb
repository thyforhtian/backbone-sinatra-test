require "bundler/capistrano"

set :application, "sin"
# set :repository,  "file://."
set :repository,  "git@bitbucket.org:thyforhtian/backbone-sinatra-test.git"

set :deploy_to, "/usr/share/nginx/html/#{application}"
set :deploy_via, :remote_cache
# set :deploy_via, :copy
set :user, "deployer"
set :group, "nginx" 
set :use_sudo, false

set :default_environment, { 'PATH' => "/home/deployer/.rbenv/shims:/home/deployer/.rbenv/bin:$PATH" }
set :bundle_flags, "--deployment --quiet --binstubs --shebang ruby-local-exec"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "luchowski.net"                          # Your HTTP server, Apache/etc
role :app, "luchowski.net"                          # This may be the same as your `Web` server
role :db,  "luchowski.net", :primary => true # This is where Rails migrations will run

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end

# namespace :deploy do
	
# end

namespace :god do
	task :start do
		run "cd /usr/share/nginx/html/sin/current && god -c config/sin.god"
	end

	task :restart do
		sleep 5
		run "cd /usr/share/nginx/html/sin/current && god restart sin"
	end
end

after "deploy:update", "god:restart"
